{
    "id": "b77fab3f-251b-457e-a27e-c4c77b84a2e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 1,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18adb61a-a450-4e7d-a43b-266a535c2e90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b77fab3f-251b-457e-a27e-c4c77b84a2e2",
            "compositeImage": {
                "id": "7ac7ae80-5e8b-4a37-976a-d863a36dba1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18adb61a-a450-4e7d-a43b-266a535c2e90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adbc5943-a7c0-471b-8268-4fc304033a44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18adb61a-a450-4e7d-a43b-266a535c2e90",
                    "LayerId": "f06c7956-2fff-4aa7-8980-bef57db28049"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "f06c7956-2fff-4aa7-8980-bef57db28049",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b77fab3f-251b-457e-a27e-c4c77b84a2e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 13
}