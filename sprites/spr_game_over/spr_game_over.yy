{
    "id": "a1b568ae-cd47-415f-9482-cbd847927ec1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_game_over",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 0,
    "bbox_right": 347,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64e309e4-ede8-4314-b0d4-3a8942872528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1b568ae-cd47-415f-9482-cbd847927ec1",
            "compositeImage": {
                "id": "c42db3f5-1c80-4e0e-8e66-189727a51a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64e309e4-ede8-4314-b0d4-3a8942872528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c3d4b15-0159-4397-b39b-b718cab48728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64e309e4-ede8-4314-b0d4-3a8942872528",
                    "LayerId": "5d7cb77d-154f-487a-a28d-4e7868043b18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "5d7cb77d-154f-487a-a28d-4e7868043b18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1b568ae-cd47-415f-9482-cbd847927ec1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 349,
    "xorig": 174,
    "yorig": 36
}