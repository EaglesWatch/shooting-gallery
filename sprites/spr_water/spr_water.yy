{
    "id": "d4794516-c608-4c3c-a385-1681ad02755a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 131,
    "bbox_top": 568,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8afac022-8a20-48c9-a08d-6cadb4e4edc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4794516-c608-4c3c-a385-1681ad02755a",
            "compositeImage": {
                "id": "0d62f8d0-0318-4d44-85e1-9178031099fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8afac022-8a20-48c9-a08d-6cadb4e4edc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06a41d97-cefb-4d80-bda0-00da98df3710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8afac022-8a20-48c9-a08d-6cadb4e4edc6",
                    "LayerId": "46e751c5-1d6f-463c-aa09-3cee930340d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "46e751c5-1d6f-463c-aa09-3cee930340d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4794516-c608-4c3c-a385-1681ad02755a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 132,
    "xorig": 0,
    "yorig": 0
}