{
    "id": "bf598a78-c5e4-4978-ac32-f381d3a2981a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_duck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 0,
    "bbox_right": 113,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84e5e6cd-dba4-4f51-9ef4-1f63c23369bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf598a78-c5e4-4978-ac32-f381d3a2981a",
            "compositeImage": {
                "id": "bc27d6d1-ba3b-44a3-b46a-46596e9853f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84e5e6cd-dba4-4f51-9ef4-1f63c23369bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cb2877f-6db2-451c-9e6a-bd6d45eb637b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84e5e6cd-dba4-4f51-9ef4-1f63c23369bf",
                    "LayerId": "9904314e-af75-411d-b6d9-38ce31303fee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "9904314e-af75-411d-b6d9-38ce31303fee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf598a78-c5e4-4978-ac32-f381d3a2981a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 108
}