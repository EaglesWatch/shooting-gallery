{
    "id": "091efe4d-9f3f-4538-9e0d-6bbbf35bd766",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_target",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 141,
    "bbox_left": 0,
    "bbox_right": 141,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a2bfb99-03ea-43df-9950-9d9561b29034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "091efe4d-9f3f-4538-9e0d-6bbbf35bd766",
            "compositeImage": {
                "id": "6bc7d1fc-648f-47ba-b357-779fca68be18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a2bfb99-03ea-43df-9950-9d9561b29034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62ceb6a8-8906-4d8c-a5ca-39e82ac035d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a2bfb99-03ea-43df-9950-9d9561b29034",
                    "LayerId": "ad4fbe78-94fb-4a8b-9c32-f64ddccc39a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 142,
    "layers": [
        {
            "id": "ad4fbe78-94fb-4a8b-9c32-f64ddccc39a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "091efe4d-9f3f-4538-9e0d-6bbbf35bd766",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 142,
    "xorig": 71,
    "yorig": 71
}