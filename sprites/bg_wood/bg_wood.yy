{
    "id": "487f4289-d1a9-48c4-a9c4-abc49f76e153",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_wood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e229c79d-784c-431d-8def-a294a1ab03a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "487f4289-d1a9-48c4-a9c4-abc49f76e153",
            "compositeImage": {
                "id": "ab7a4c66-0a5d-4b9a-b198-a78dcf2fe29b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e229c79d-784c-431d-8def-a294a1ab03a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "631b92e0-1464-4e1d-b19f-e81375bc051f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e229c79d-784c-431d-8def-a294a1ab03a5",
                    "LayerId": "1113643e-61f6-4aa7-8acd-cb26f9963124"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1113643e-61f6-4aa7-8acd-cb26f9963124",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "487f4289-d1a9-48c4-a9c4-abc49f76e153",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}