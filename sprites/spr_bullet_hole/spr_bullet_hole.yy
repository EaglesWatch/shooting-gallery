{
    "id": "d28cecfa-8cd0-4dbf-bbde-1187887aded5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet_hole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6e3960a-0207-45bb-b44c-a4cec0c646c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d28cecfa-8cd0-4dbf-bbde-1187887aded5",
            "compositeImage": {
                "id": "a2bb45fd-79fe-449a-bd75-3347dc166c9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6e3960a-0207-45bb-b44c-a4cec0c646c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef2335e6-e213-429f-8c97-35ec777c3dae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6e3960a-0207-45bb-b44c-a4cec0c646c6",
                    "LayerId": "98f319e6-be06-4487-8b26-a7f7d5d6c754"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "98f319e6-be06-4487-8b26-a7f7d5d6c754",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d28cecfa-8cd0-4dbf-bbde-1187887aded5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}