{
    "id": "5731137a-746d-4ab9-90f5-26284dfbffab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_curtain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41390bd3-5398-4a6c-94f4-630d94192ecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5731137a-746d-4ab9-90f5-26284dfbffab",
            "compositeImage": {
                "id": "39f3a7bb-c445-42e5-a141-da2b8bd721e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41390bd3-5398-4a6c-94f4-630d94192ecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c486073-93a4-4a9b-8d24-b39f9f5dd30f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41390bd3-5398-4a6c-94f4-630d94192ecc",
                    "LayerId": "32f1833a-c76e-4ca5-8c53-9ce47f181f40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "32f1833a-c76e-4ca5-8c53-9ce47f181f40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5731137a-746d-4ab9-90f5-26284dfbffab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}