{
    "id": "f19ba70d-577c-4f07-b0da-eb21e500b67e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_target_duck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 34,
    "bbox_right": 73,
    "bbox_top": 56,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9442aa6f-9f35-48cd-8102-14a634dab533",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f19ba70d-577c-4f07-b0da-eb21e500b67e",
            "compositeImage": {
                "id": "ab0cef85-a958-48e6-943a-f4c9fe1352d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9442aa6f-9f35-48cd-8102-14a634dab533",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34990ad1-7cd3-4ae6-ba95-a0de1ce1d6aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9442aa6f-9f35-48cd-8102-14a634dab533",
                    "LayerId": "f8ef4493-6aaa-4650-aaeb-e38a10224584"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "f8ef4493-6aaa-4650-aaeb-e38a10224584",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f19ba70d-577c-4f07-b0da-eb21e500b67e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 108
}