{
    "id": "5d9c2661-d319-4635-a48e-1f4ef76cad91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a16eeec-3ee2-41c2-a2db-683f731df3fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d9c2661-d319-4635-a48e-1f4ef76cad91",
            "compositeImage": {
                "id": "6d1b2204-39d2-4012-9437-f85a129f7d9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a16eeec-3ee2-41c2-a2db-683f731df3fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34fb7450-f79c-4988-aaff-c937dfca2413",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a16eeec-3ee2-41c2-a2db-683f731df3fb",
                    "LayerId": "cf02053b-bf02-4496-b340-f6e90e95059b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cf02053b-bf02-4496-b340-f6e90e95059b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d9c2661-d319-4635-a48e-1f4ef76cad91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}