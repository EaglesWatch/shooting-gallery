{
    "id": "99109e50-d8f7-45d1-8843-3d2a5722a280",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_reticule",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1679a59-0b46-478b-87ea-3079700ac363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99109e50-d8f7-45d1-8843-3d2a5722a280",
            "compositeImage": {
                "id": "d9abbdb5-872a-499b-82f7-a3cee1df1ed5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1679a59-0b46-478b-87ea-3079700ac363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7372366-8b7c-4d7f-a0ed-2adce87feadf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1679a59-0b46-478b-87ea-3079700ac363",
                    "LayerId": "5254fb50-2032-44e2-ab12-3df1fd548f1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "5254fb50-2032-44e2-ab12-3df1fd548f1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99109e50-d8f7-45d1-8843-3d2a5722a280",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 25
}