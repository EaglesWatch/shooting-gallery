{
    "id": "9e1e20f9-3c0b-40d3-a166-c4dcfa595be9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 141,
    "bbox_left": 0,
    "bbox_right": 141,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e534424c-6345-4ba9-afeb-b65c39874070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e1e20f9-3c0b-40d3-a166-c4dcfa595be9",
            "compositeImage": {
                "id": "b6e8a2a7-e465-493c-ba28-5878506e4cc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e534424c-6345-4ba9-afeb-b65c39874070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03187713-7e4c-435a-a025-745099a416e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e534424c-6345-4ba9-afeb-b65c39874070",
                    "LayerId": "ab347cbc-f275-40dd-b883-300d6aae1bd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 142,
    "layers": [
        {
            "id": "ab347cbc-f275-40dd-b883-300d6aae1bd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e1e20f9-3c0b-40d3-a166-c4dcfa595be9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 142,
    "xorig": 71,
    "yorig": 71
}