/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 39ECBFB6
/// @DnDArgument : "xpos_relative" "1"
/// @DnDArgument : "ypos_relative" "1"
/// @DnDArgument : "objectid" "obj_bullet_hole"
/// @DnDSaveInfo : "objectid" "86b7ba34-8829-4bea-8604-220638968763"
instance_create_layer(x + 0, y + 0, "Instances", obj_bullet_hole);

/// @DnDAction : YoYo Games.Instance Variables.Set_Lives
/// @DnDVersion : 1
/// @DnDHash : 4D6630C3
/// @DnDApplyTo : 424fbab9-e440-4ea8-b24e-ca64f5787829
/// @DnDArgument : "lives" "-1"
/// @DnDArgument : "lives_relative" "1"
with(obj_controller) {
if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
__dnd_lives += real(-1);
}