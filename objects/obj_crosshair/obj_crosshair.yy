{
    "id": "45c86d1f-dcc7-4fb1-bbde-ea59cbcbc323",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crosshair",
    "eventList": [
        {
            "id": "d015d8a2-bddc-4991-abb9-81060babd0fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "45c86d1f-dcc7-4fb1-bbde-ea59cbcbc323"
        },
        {
            "id": "76896cf6-4fcf-45f9-bce2-5bdcd239b372",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "45c86d1f-dcc7-4fb1-bbde-ea59cbcbc323"
        },
        {
            "id": "79db368c-d6bb-4de8-b6ec-c8157a5930b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "45c86d1f-dcc7-4fb1-bbde-ea59cbcbc323"
        },
        {
            "id": "66733277-f841-46f5-88eb-050e1de8b7a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "45c86d1f-dcc7-4fb1-bbde-ea59cbcbc323"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "99109e50-d8f7-45d1-8843-3d2a5722a280",
    "visible": true
}