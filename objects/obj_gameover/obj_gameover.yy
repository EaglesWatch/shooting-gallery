{
    "id": "b0940ca2-90af-40ec-aa74-b412afe69326",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameover",
    "eventList": [
        {
            "id": "0ce66de8-910b-4a8d-9687-3cabb8c9fbb0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b0940ca2-90af-40ec-aa74-b412afe69326"
        },
        {
            "id": "a5f28ffe-3552-4087-baeb-b466784068f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "b0940ca2-90af-40ec-aa74-b412afe69326"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a1b568ae-cd47-415f-9482-cbd847927ec1",
    "visible": true
}