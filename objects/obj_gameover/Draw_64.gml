/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 1812CED7
draw_self();

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 197196D6
/// @DnDArgument : "x" "450"
/// @DnDArgument : "y" "425"
/// @DnDArgument : "caption" ""End Score: ""
/// @DnDArgument : "var" "global.end_score"
draw_text(450, 425, string("End Score: ") + string(global.end_score));

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 42F06DB8
/// @DnDArgument : "x" "440"
/// @DnDArgument : "y" "450"
/// @DnDArgument : "caption" ""Press R to restart ""
draw_text(440, 450, string("Press R to restart ") + "");