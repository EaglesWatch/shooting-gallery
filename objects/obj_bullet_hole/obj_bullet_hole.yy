{
    "id": "86b7ba34-8829-4bea-8604-220638968763",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_hole",
    "eventList": [
        {
            "id": "34c0ef3c-9c85-4972-a3a0-431f725becef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "86b7ba34-8829-4bea-8604-220638968763"
        },
        {
            "id": "81be5b3c-1ed9-4bd2-aeab-8c07064f0685",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "86b7ba34-8829-4bea-8604-220638968763"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d28cecfa-8cd0-4dbf-bbde-1187887aded5",
    "visible": true
}