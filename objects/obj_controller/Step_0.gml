/// @DnDAction : YoYo Games.Instance Variables.If_Lives
/// @DnDVersion : 1
/// @DnDHash : 7394FDBA
/// @DnDArgument : "op" "3"
if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
if(__dnd_lives <= 0)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 49B74206
	/// @DnDParent : 7394FDBA
	/// @DnDArgument : "value" "obj_controller.__dnd_score"
	/// @DnDArgument : "var" "end_score"
	global.end_score = obj_controller.__dnd_score;

	/// @DnDAction : YoYo Games.Rooms.Go_To_Room
	/// @DnDVersion : 1
	/// @DnDHash : 2E49D1B4
	/// @DnDParent : 7394FDBA
	/// @DnDArgument : "room" "room_game_end"
	/// @DnDSaveInfo : "room" "6556b36c-4fc0-4323-86a9-ed9f91aba2bc"
	room_goto(room_game_end);
}