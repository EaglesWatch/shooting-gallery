/// @DnDAction : YoYo Games.Timelines.Set_Timeline
/// @DnDVersion : 1
/// @DnDHash : 41095E3C
/// @DnDArgument : "timeline" "timeline_room_main"
/// @DnDArgument : "loop" "1"
/// @DnDSaveInfo : "timeline" "8b4d9f4a-23cd-43b8-958a-dfe30d3c13b1"
timeline_index = timeline_room_main;
timeline_loop = 1;
timeline_running = 1;

/// @DnDAction : YoYo Games.Instance Variables.Set_Score
/// @DnDVersion : 1
/// @DnDHash : 06D1776C

__dnd_score = real(0);

/// @DnDAction : YoYo Games.Instance Variables.Set_Lives
/// @DnDVersion : 1
/// @DnDHash : 757CED65
/// @DnDArgument : "lives" "8"

__dnd_lives = real(8);