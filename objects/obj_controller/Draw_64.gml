/// @DnDAction : YoYo Games.Drawing.Set_Color
/// @DnDVersion : 1
/// @DnDHash : 1C08E2EC
draw_set_colour($FFFFFFFF & $ffffff);
draw_set_alpha(($FFFFFFFF >> 24) / $ff);

/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 3BDE775E
/// @DnDArgument : "font" "font_in_game"
/// @DnDSaveInfo : "font" "d8918f6b-5483-4425-83c9-bbef66ec6cd2"
draw_set_font(font_in_game);

/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Score
/// @DnDVersion : 1
/// @DnDHash : 17E46C84
/// @DnDArgument : "x" "50"
/// @DnDArgument : "y" "10"
if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
draw_text(50, 10, string("Score: ") + string(__dnd_score));

/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Lives
/// @DnDVersion : 1
/// @DnDHash : 7E92A4B7
/// @DnDArgument : "x" "150"
/// @DnDArgument : "y" "17"
/// @DnDArgument : "sprite" "spr_bullet"
/// @DnDSaveInfo : "sprite" "b77fab3f-251b-457e-a27e-c4c77b84a2e2"
var l7E92A4B7_0 = sprite_get_width(spr_bullet);
var l7E92A4B7_1 = 0;
if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
for(var l7E92A4B7_2 = __dnd_lives; l7E92A4B7_2 > 0; --l7E92A4B7_2) {
	draw_sprite(spr_bullet, 0, 150 + l7E92A4B7_1, 17);
	l7E92A4B7_1 += l7E92A4B7_0;
}