{
    "id": "a8f5fb0e-d23d-4438-9012-99d473d9b3be",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Duck",
    "eventList": [
        {
            "id": "9f9d0cb9-38ed-4882-94bb-54e00b802250",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a8f5fb0e-d23d-4438-9012-99d473d9b3be"
        },
        {
            "id": "bfeacb81-2d72-4f1e-937b-f592035cf67a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a8f5fb0e-d23d-4438-9012-99d473d9b3be"
        },
        {
            "id": "887f38f9-1f07-4d6a-bb1e-755249f3aa8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "a8f5fb0e-d23d-4438-9012-99d473d9b3be"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bf598a78-c5e4-4978-ac32-f381d3a2981a",
    "visible": true
}