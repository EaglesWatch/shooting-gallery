{
    "id": "613a3525-4687-4f89-aca2-27f153c224bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_duck_target",
    "eventList": [
        {
            "id": "775b7920-b7eb-4ff1-8053-59162efe4270",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "613a3525-4687-4f89-aca2-27f153c224bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a8f5fb0e-d23d-4438-9012-99d473d9b3be",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f19ba70d-577c-4f07-b0da-eb21e500b67e",
    "visible": true
}