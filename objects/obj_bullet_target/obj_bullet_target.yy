{
    "id": "d5004384-a5d2-4cc6-9761-847de33bd1bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_target",
    "eventList": [
        {
            "id": "ac1bb8b1-c196-4f5c-9f9a-e54947008977",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d5004384-a5d2-4cc6-9761-847de33bd1bf"
        },
        {
            "id": "84087287-9229-43ea-bb41-a2eccc006355",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d5004384-a5d2-4cc6-9761-847de33bd1bf"
        },
        {
            "id": "21e16cb3-f97b-46ad-ac94-27186f1638bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "d5004384-a5d2-4cc6-9761-847de33bd1bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9e1e20f9-3c0b-40d3-a166-c4dcfa595be9",
    "visible": true
}