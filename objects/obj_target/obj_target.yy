{
    "id": "07aa7cff-2355-4550-9d87-753e304d464c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_target",
    "eventList": [
        {
            "id": "175ec422-6510-40ad-88c6-5687b8189381",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "07aa7cff-2355-4550-9d87-753e304d464c"
        },
        {
            "id": "20c17e99-be20-48b2-8d68-2581f3c97df5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "07aa7cff-2355-4550-9d87-753e304d464c"
        },
        {
            "id": "7b719111-0c7d-46c9-8b3a-a65a6c257268",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "07aa7cff-2355-4550-9d87-753e304d464c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "091efe4d-9f3f-4538-9e0d-6bbbf35bd766",
    "visible": true
}