/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 460F86C4
/// @DnDArgument : "xpos" "960"
/// @DnDArgument : "ypos" "irandom_range(100, 400)"
/// @DnDArgument : "objectid" "obj_bullet_target"
/// @DnDSaveInfo : "objectid" "d5004384-a5d2-4cc6-9761-847de33bd1bf"
instance_create_layer(960, irandom_range(100, 400), "Instances", obj_bullet_target);

/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 13074ABB
/// @DnDArgument : "xpos" "60"
/// @DnDArgument : "ypos" "irandom_range(100, 400)"
/// @DnDArgument : "objectid" "obj_target"
/// @DnDSaveInfo : "objectid" "07aa7cff-2355-4550-9d87-753e304d464c"
instance_create_layer(60, irandom_range(100, 400), "Instances", obj_target);