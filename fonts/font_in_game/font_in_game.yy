{
    "id": "d8918f6b-5483-4425-83c9-bbef66ec6cd2",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_in_game",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Elephant",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "664f4566-27c5-4761-8966-9ff37201f86f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 7,
                "y": 74
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2b6e9432-38ea-4deb-a3d0-8332adab8cca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 229,
                "y": 56
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bd53d6fb-e0b9-4b9e-8592-1784ae82f7be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 183,
                "y": 56
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f96029df-c1eb-4bb2-aa75-bf572ec2ead9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 147,
                "y": 20
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a1458a29-8056-459c-9639-337b1f17ce53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 38
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "bf771b78-03fe-49fa-8f5f-24b8559fcb81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d1e01e20-8552-456f-92b7-c88c856360d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3e0eeb92-2498-4082-850d-9bca9aca11c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f208b8a6-23a6-419f-a69b-b9a48a4cfc26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 167,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "05841fe8-3994-4f65-9f2d-ec92251a53e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 151,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9c4cc189-60e5-407f-b572-d4362b9e8858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 143,
                "y": 56
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "587237ce-ca27-4a3c-a0b5-4b85f7c8c594",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 123,
                "y": 38
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "006a24ce-7900-465d-b309-e226b42ac693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 211,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9083315b-9f0b-4a81-9c68-1db1ae2cffbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 247,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ee926268-9b64-4180-afe5-737a3cdf8aea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 241,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9ceb3907-8cf9-4dca-a965-cd5b0177b444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 111,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e193a6be-a0cd-4024-9b8b-55fb265cf7bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 135,
                "y": 20
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8e9b3e45-e795-42af-b1a4-c972424d81fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 102,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3fc1de30-0239-4900-abd8-7ed5e3db9fcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 189,
                "y": 38
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "db99220c-bd73-4983-b21c-b08c5f81514f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 38
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "750fc672-710b-4ffd-94cc-ac5277a7741b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 231,
                "y": 20
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b3fe848a-f7d6-414b-a921-43ece7cdf100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 38
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b44c7a63-2862-4c65-a37d-5bb1e9f35bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 38
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "26b31386-9eaf-4ec7-ae4a-232b95e53e84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 10,
                "x": 219,
                "y": 20
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4b66d172-42c2-4343-b145-dbb59804b52a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 38
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5513c72c-9481-4259-ac16-477708173046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 134,
                "y": 38
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8835e8ed-a162-4ed0-b8bd-9f28ea1d6d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 217,
                "y": 56
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "164ae0f7-351a-46c1-8735-57e86e8cf99e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 235,
                "y": 56
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "bef02753-d99e-4265-b7be-44023b420faf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 112,
                "y": 38
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ad76af8f-b709-43e3-bd57-5b12292d9544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 101,
                "y": 38
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "18f70fcf-c6ce-4800-a270-b08f22a35f46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 90,
                "y": 38
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "234a1e82-8339-457f-9503-39cc24735b43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 220,
                "y": 38
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "410a6f31-26e3-49e9-9864-414a01c1b925",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "893c0ce3-6100-4168-a088-363190c6af6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "95b05cc6-2750-4e56-962f-ac811c35e446",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 96,
                "y": 20
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "042e3cf5-e99c-42c5-8ea6-598923288ad1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 159,
                "y": 20
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ccae0b1b-a731-4cd4-bc8b-ebcfd6f98b56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e8bc4d8f-ee4a-4785-a2dc-88b489bfe353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 70,
                "y": 20
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5cfe5957-258d-49a0-87d9-0ad9d70c1dc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 57,
                "y": 20
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c2a11672-9d94-4970-8088-168e28a773ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fc340509-d1b2-40c3-96f8-39a5660e3d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "63e0a209-1d86-427a-b24d-d45582493af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 48,
                "y": 56
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "202773c9-376e-4279-aab1-bdaebf0cd1b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 183,
                "y": 20
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "578fb716-8bcd-4350-be4d-f698366aa43b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "70182b21-394c-453f-b57b-1b5c7c9ea54d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 122,
                "y": 20
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2120455d-8786-44cc-a708-0d9362ae2587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "33ff5516-0f54-469d-be84-61109730b865",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2483932f-3c5c-46dd-a7a6-ce6d6e48c2af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9f609d36-38da-407b-9438-6387c330d72d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 109,
                "y": 20
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "661655bd-107f-4aa8-ad7b-c167a45fd6bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8791888f-8ccc-429d-8b58-c3f1b4b84bf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "37510b0e-9d07-402c-b183-650eeb3a2b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "cfb2ea60-18f6-4612-a252-18fea6cbd9c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 83,
                "y": 20
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ea336859-425a-4c20-b309-bbaefd1c568c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7ff47cf8-2b3a-406b-9261-3f19a3042fb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a5b63cf7-0023-4a9b-90dd-0bca60ad9028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3fdac2b3-a6fd-4773-8a93-be6ad80c8fe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7360c9b0-942e-4b3f-94c2-414db022cfc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 30,
                "y": 20
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8f3d0b59-b85e-41fc-9250-3c2b9ebebb55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 44,
                "y": 20
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7a71ad0f-7d46-4856-a758-a849bf6daad7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 175,
                "y": 56
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3f9c2beb-dd98-4ab7-9e5d-330802547bb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 75,
                "y": 56
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "906a1db1-4ff0-458c-9dea-032f6204e18f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 127,
                "y": 56
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5739b59d-1329-429e-8631-86ae7719e008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 93,
                "y": 56
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "975d4674-96bf-4d0c-9d80-fb321e627522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 84,
                "y": 56
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "79286993-d2f4-4914-a80e-7bfd1ced4096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 223,
                "y": 56
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b4998125-0336-44ad-8545-94c81c727b80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "39608ca7-b916-4230-b4d7-60929bde4dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 24,
                "y": 38
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "eb29c079-41ab-4737-be3c-cecaff8feb46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 57,
                "y": 56
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8f31e923-bbb9-4ac0-9574-cb8e10d34114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 13,
                "y": 38
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "efb1ac58-1866-44f2-a04d-5290dc67b20d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 66,
                "y": 56
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b23d06df-fb1a-4ad2-b86b-fe43ac04d663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 7,
                "x": 39,
                "y": 56
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ea550e69-e50a-4f51-a4c1-74ea2ea27604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 200,
                "y": 38
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "38534b08-08ee-42f8-ac3e-487c8a05dc1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 207,
                "y": 20
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6172b23a-bc19-458a-9689-114ed05873dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 204,
                "y": 56
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4b158ff0-c915-41f5-9cef-e9f0481a7e15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 16,
                "offset": -2,
                "shift": 4,
                "w": 6,
                "x": 119,
                "y": 56
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "701dd151-1d26-4070-9deb-5d76199c24ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 195,
                "y": 20
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "901fcfd9-6090-422b-a179-50911df0b049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 135,
                "y": 56
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0df33b1c-2128-483b-8ee4-2901fdbe4af9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2f33a753-c063-4f0d-8e96-856696828798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 46,
                "y": 38
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3cee4b5b-ddd8-431a-bfc5-de423985fe37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 210,
                "y": 38
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6d87e960-5e49-4b5f-8415-ec6b7456d1d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 57,
                "y": 38
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f80e78ca-bc5e-400a-8c63-41f267a430dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 243,
                "y": 20
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d1cd9c17-204a-43e9-a983-02726fe89042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 21,
                "y": 56
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "61e79dea-b5fa-49bb-b2cf-312b1c7b6e98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 159,
                "y": 56
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c6b0ccd6-0d51-468d-8f8c-f26be5c91778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 12,
                "y": 56
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8de16a81-5582-4a0b-8c3e-54a152173115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 171,
                "y": 20
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3b4b2b06-70a1-48c2-996d-5a0eabb69098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 230,
                "y": 38
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "07eb58aa-ba6f-4a1b-a4d1-184468df3075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 16,
                "y": 20
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2a125e35-67dd-4c89-855d-191ab6c69dc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 35,
                "y": 38
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b48a72d4-4a1a-491a-b530-3cc1ba5cf4aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 240,
                "y": 38
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ae61c12d-6a39-407d-8439-b93ceae801ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 30,
                "y": 56
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b5ebac03-3781-4461-a6f1-41a62c067321",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 190,
                "y": 56
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "27cb37aa-e002-4941-984b-326af1c68486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 16,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 12,
                "y": 74
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "28e3c968-55d2-49d4-b57c-3662124fbf15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 197,
                "y": 56
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "50963040-9abf-49a0-9911-8ba4cb256270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 68,
                "y": 38
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "af305bd9-f3ca-4eab-ad0e-868a36447780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 44
        },
        {
            "id": "38191f30-9ded-4273-b4e9-760f1522e793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 46
        },
        {
            "id": "0209f2b5-02c2-4e9b-b427-57e83b3960ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 198
        },
        {
            "id": "97cca432-1955-49be-98c2-35962cdc9001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 44
        },
        {
            "id": "4872cea3-59b7-45de-8d08-31934ee43c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 46
        },
        {
            "id": "59d37b4a-aab4-4ff0-9ff9-4d3dd0201dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 198
        },
        {
            "id": "40e05e7c-fd88-4c8c-8cf5-e73e2a2789d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 34
        },
        {
            "id": "fc99151b-747a-4ead-b399-85e264e88532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 39
        },
        {
            "id": "22236d4a-7f0c-41aa-8b96-db479561856a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8217
        },
        {
            "id": "5e43a74b-1ee6-4239-94aa-f77d2ffcf48d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8221
        },
        {
            "id": "2e5416ee-c2f8-491a-bfb8-67c31f113a4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 34
        },
        {
            "id": "3e0515bd-439d-4d4e-8756-c0314f38bba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 39
        },
        {
            "id": "683d7e31-7cca-4263-b9a0-350821f262f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8217
        },
        {
            "id": "71d6e224-95ec-4bfc-9881-963083409827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8221
        },
        {
            "id": "29632066-bbd7-4a6d-a87f-afa6131938ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 44
        },
        {
            "id": "3ab3455a-1a03-4900-ad1d-445ffad6b670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 46
        },
        {
            "id": "bf7efa5c-4544-4c4f-b00b-380c6b1bda50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 52
        },
        {
            "id": "df5f06f2-f0e7-487f-865d-5be4e4dddf6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 93
        },
        {
            "id": "ed084c10-6533-4265-94e2-91f0a6cdc537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 34
        },
        {
            "id": "388fec0b-9264-4bb2-ad6a-4e8ceaaf51b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "bf1b59c3-ca1c-42a2-8542-8365e2a2489c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "36df12d6-d2d9-4bc0-9a65-ba910d56e950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "1ae4c182-4180-4cb0-9491-5b8304d1b701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "1d3b5d39-b0fe-4828-be64-41a2fc0c1e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "ffd6de24-6f14-4fd2-9ea1-ef5633059072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "aa4b742f-bcf2-49eb-8307-7f52be0cf15e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 305
        },
        {
            "id": "6f6629d4-b8b6-458e-8582-668c6eb6ac1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8218
        },
        {
            "id": "b2467129-4f8e-4bff-b67f-c4189eba97ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8222
        },
        {
            "id": "9359951c-7c3d-4f31-acc4-2765ab349f29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "291eae17-ae4c-41b5-b15d-995a36bf5a7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "7a44e7a3-260a-4c0f-93e3-ba59bb34cdb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "d5f41d50-8917-4d66-99a7-2a18d84dc620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "c2d9ab4c-1a02-404d-8c2a-6447a4e85f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "49f352a5-c3d8-4611-9edc-5415ca6a75ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "41207db9-d6d0-4123-90ae-1cd17ca4ec6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8218
        },
        {
            "id": "8816fc74-5849-483b-8790-9e676472f056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8222
        },
        {
            "id": "2dbdbadc-73b7-4fe0-acf9-6d2abe079a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "0992d1b0-58ec-4fc2-a793-2fa8231e168d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "6b75351d-6ecf-4ae1-833f-e28373244827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 305
        },
        {
            "id": "982f3d5d-ed91-4e59-b019-c596b5b78ec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8218
        },
        {
            "id": "475edf34-8c4b-4c0c-aa9c-4f36a6dffb76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8222
        },
        {
            "id": "09eea4d2-5a76-427e-b635-50f94080d11a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "e936a8b1-1258-417b-b1db-a440f61e65d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "5ab9c43b-a5c8-48c3-9e0e-c33364376270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "c25731ca-a12a-4117-8e2b-f4f46c8e6add",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "f9176d76-76ec-431e-b1c6-a7e1d99e7589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "57167cae-a4b4-462e-814f-a242cb6d7614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 230
        },
        {
            "id": "3116df30-7ed3-4d40-9102-e9d14b61f386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "c59d0eaf-67c4-46c1-9424-2c2ec38eabff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "c7542aeb-3fc7-4e3a-ac5f-c9feda2b696d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8218
        },
        {
            "id": "490804d5-5ad4-4399-9271-89873368c7c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8222
        },
        {
            "id": "1538988e-c5a6-4e39-8cbb-346d2952a01a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "0ec2b882-e50c-4492-b137-ced5fddcf958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "6deb6c19-7b5b-4117-9a08-59d672c9e3bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "df90d511-c345-4938-98dd-56d2907f4e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "4456d1ce-16c2-4df1-b15c-26d50fee024d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "e89a6aa7-2f11-4d87-8ae6-4a80f86b3b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "d1a43a24-7ff1-4a2e-80d2-078241f3a217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "9c6ea9f2-f50f-481a-913b-aa0b5b5ed045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "0e2c619d-622f-47a0-9aba-17ebf0cf10f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8218
        },
        {
            "id": "61f07a6b-113d-4810-89a6-c1504ac89076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8222
        },
        {
            "id": "d63f98cb-4238-49d6-90a6-f78aa0721e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 34
        },
        {
            "id": "5cb4863a-c2b6-4573-936f-24d34dc6b931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 39
        },
        {
            "id": "17cf25b7-d376-4de6-b484-2fb4ba59cc8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 41
        },
        {
            "id": "e0f8ef49-1b76-4cf9-8dd7-e7d255a187a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 93
        },
        {
            "id": "d4fe3a3f-cebe-43ed-9b86-668290e0da4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 125
        },
        {
            "id": "b6d88aec-1505-429c-a9b5-d05922bfb014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "c1079cc7-3bc2-4019-9cf1-e8dd79268cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "575caa49-ddbf-42ef-b5a0-f5fb742b8e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 106
        },
        {
            "id": "5fab1203-16ad-4de6-9a7b-6f4efc7e6dbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "aaaef236-72e9-4b2c-903b-0b4e72e68a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}